<!------ Include the above in your HEAD tag ---------->
<style>
    [data-toggle="collapse"]:after {
        display: inline-block;
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        position: absolute;
        top: 20px;
        right: 10px;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        content: "\f054";
        transform: rotate(90deg);
        transition: all linear 0.25s;
        float: right;
    }

    [data-toggle="collapse"].collapsed:after {
        transform: rotate(0deg);
    }

    .aside_top a {
        text-decoration: none !important;
    }

    .aside_top {
        background-color: #fff;
        border-radius: 12px 12px 0px 0px !important;
    }

    .aside_bottom {
        background-color: #fff;
        border-radius: 0px 0px 12px 12px !important;
    }

    .aside_body {
        /*width: 318px !important;*/
        height: auto !important;
        background: #F7F7F7 !important;
    }
</style>

@if($user->role!= user_roles('3'))
<div class="row mb-2">
    <div class="col-lg-8">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text bg-white" style="border-right: none; border: 1px solid #DDDDDD;">
                    <svg width="11" height="15" viewBox="0 0 11 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M7.56221 14.0648C7.58971 14.3147 7.52097 14.5814 7.36287 14.7563C7.29927 14.8336 7.22373 14.8949 7.14058 14.9367C7.05742 14.9785 6.96827 15 6.87825 15C6.78822 15 6.69907 14.9785 6.61592 14.9367C6.53276 14.8949 6.45722 14.8336 6.39363 14.7563L3.63713 11.4151C3.56216 11.3263 3.50516 11.2176 3.47057 11.0977C3.43599 10.9777 3.42477 10.8496 3.43779 10.7235V6.45746L0.145116 1.34982C0.0334875 1.17612 -0.0168817 0.955919 0.005015 0.737342C0.0269117 0.518764 0.119294 0.319579 0.261975 0.183308C0.392582 0.0666576 0.536937 0 0.688166 0H10.3118C10.4631 0 10.6074 0.0666576 10.738 0.183308C10.8807 0.319579 10.9731 0.518764 10.995 0.737342C11.0169 0.955919 10.9665 1.17612 10.8549 1.34982L7.56221 6.45746V14.0648ZM2.09047 1.66644L4.81259 5.88254V10.4819L6.1874 12.1484V5.8742L8.90953 1.66644H2.09047Z" fill="#323C47" />
                        </svg>
                    </div>
                    </div>
                    @php
                        // Create an array to keep track of encountered names
                        $encounteredNames = [];
                    @endphp

                    <select name="" class="form-select" id="statusSelect" style="border-left: none;">
                        <option value="">
                            <!-- @lang('lang.filter_by_client') -->
                            Filter by {{($user->role == 'Admin') ? 'Clients' : 'Drivers'}}
                        </option>
                        @foreach($activeRoutes as $key => $value)
                            @php
                                $name = $value['driver']['name'] ?? $value['user']['name'];

                                if (!in_array($name, $encounteredNames)) {
                                    $encounteredNames[] = $name;
                            @endphp
                                    <option value="{{ $name }}">{{ $name }}</option>
                            @php
                                }
                            @endphp
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-4 pt-2">
                <span>
                    <a class="" style="font-size: small; color: #ACADAE;" href="/routes">@lang('lang.show_more')..</a>
                </span>
            </div>
            <div class="col-lg-12">
                <h6 class="mb-0 mt-2">Ongoing Trips</h6>
            </div>
        </div>
@endif
        @if(isset($activeRoutes) && count($activeRoutes) > 0)
        <div style="height: 700px; overflow: auto;">
            @foreach($activeRoutes as $key => $value)
            <div class="mb-2 trip-card "  role="tablist">
                <div class="card" style="background: #FFFFFF;box-shadow: 0px 20px 50px rgba(220, 224, 249, 0.5);border-radius: 12px;">
                    <div class="card-header aside_top" role="tab" id="heading{{$key}}">
                        <h5 class="mb-0">
                        <a data-toggle="collapse" href="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
                            <div class="d-flex flex-column">
                                <span class="mb-1" style="font-style: normal;font-weight: 700;font-size: 14px;line-height: 17px;letter-spacing: 0.01em;color: #323C47;">{{$value['title'] ?? ''}}</span>
                                <span class="text-secondary text-small" style="font-style: normal;font-weight: 400;font-size: 14px;line-height: 17px;letter-spacing: 0.01em;color: #9FA2B4;">
                                    {{ date('d F, Y', strtotime($value['trip_date'])) ?? '' }}
                                </span>
                            </div>
                        </a>
                    </h5>
                </div>
                <div id="collapse{{$key}}" class="collapse{{ $key == 0 ? ' show' : '' }}" role="tabpanel" aria-labelledby="heading{{$key}}" data-parent="#accordion">
                    <div class="card-body p-2 aside_body">
                        {{$value['desc'] ?? ''}}
                    </div>
                </div>
                <div class="card-header aside_bottom" role="tab" id="">
                    <h5 class="mb-0">
                        <a data-toggle="" href="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
                            <div class="row">
                                <div class="col-10">
                                    <div class="d-flex flex-column">
                                        <span class="mb-1 user-name" style="font-style: normal;font-weight: 700;font-size: 14px;line-height: 17px;letter-spacing: 0.01em;color: #323C47;">{{ $value['driver']['name'] ?? $value['user']['name'] }}</span>
                                        <span class="text-secondary text-small" style="font-style: normal;font-weight: 400;font-size: 14px;line-height: 17px;letter-spacing: 0.01em;color: #9FA2B4;">{{$tripStatus_trans[$value['status']] ?? ''}}</span>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <form method="POST" action="/create_trip" class="mb-0">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$value['id']}}">
                                        <input type="hidden" name="dashboard_data" value="1">
                                        <button id="btn_edit_announcement" class="btn p-0">
                                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle opacity="0.1" cx="15" cy="15" r="15" fill="#ACADAE"/>
                                                <path d="M14.7167 10C10.5 10 8 15 8 15C8 15 10.5 20 14.7167 20C18.8333 20 21.3333 15 21.3333 15C21.3333 15 18.8333 10 14.7167 10ZM14.6667 11.6667C16.5167 11.6667 18 13.1667 18 15C18 16.85 16.5167 18.3333 14.6667 18.3333C12.8333 18.3333 11.3333 16.85 11.3333 15C11.3333 13.1667 12.8333 11.6667 14.6667 11.6667ZM14.6667 13.3333C13.75 13.3333 13 14.0833 13 15C13 15.9167 13.75 16.6667 14.6667 16.6667C15.5833 16.6667 16.3333 15.9167 16.3333 15C16.3333 14.8333 16.2667 14.6833 16.2333 14.5333C16.1 14.8 15.8333 15 15.5 15C15.0333 15 14.6667 14.6333 14.6667 14.1667C14.6667 13.8333 14.8667 13.5667 15.1333 13.4333C14.9833 13.3833 14.8333 13.3333 14.6667 13.3333Z" fill="#323C47"/>
                                            </svg>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </a>
                    </h5>
                </div>
                <div id="collapse{{$key}}" class="collapse{{ $key == 0 ? ' show' : '' }}" role="tabpanel" aria-labelledby="" data-parent="">
                    </div>
                </div>
            </div>
            @endforeach


@else
    <div class="mb-2" id="noTrip" role="tablist">
        <div class="card" style="background: #FFFFFF;box-shadow: 0px 20px 50px rgba(220, 224, 249, 0.5);border-radius: 12px;">
            <div class="card-header aside_top" role="tab" id="headingNoData">
                <h5 class="mb-0">
                    <a data-toggle="collapse" href="#collapseNoData" aria-expanded="true" aria-controls="collapseNoData">
                        <div class="d-flex flex-column">
                            <span class="mb-1" style="font-style: normal;font-weight: 700;font-size: 14px;line-height: 17px;letter-spacing: 0.01em;color: #323C47;">No trip available yet</span>
                        </div>
                    </a>
                </h5>
            </div>
            <div id="collapseNoData" class="collapse show" role="tabpanel" aria-labelledby="headingNoData" data-parent="#accordion">
                <div class="card-body p-2 aside_body">
                    <!-- Additional content for no data scenario if needed -->
                    <p>You don't have any trips yet!</p>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="mb-2 noTripCard" id="noTripCard" role="tablist">
        <div class="card" style="background: #FFFFFF;box-shadow: 0px 20px 50px rgba(220, 224, 249, 0.5);border-radius: 12px;">
            <div class="card-header aside_top" role="tab" id="headingNoData">
                <h5 class="mb-0">
                    <a data-toggle="collapse" href="#collapseNoData" aria-expanded="true" aria-controls="collapseNoData">
                        <div class="d-flex flex-column">
                            <span class="mb-1" style="font-style: normal;font-weight: 700;font-size: 14px;line-height: 17px;letter-spacing: 0.01em;color: #323C47;">No trip available yet</span>
                        </div>
                    </a>
                </h5>
            </div>
            <div id="collapseNoData" class="collapse show" role="tabpanel" aria-labelledby="headingNoData" data-parent="#accordion">
                <div class="card-body p-2 aside_body">
                    <!-- Additional content for no data scenario if needed -->
                    <p>You don't have any trips yet!</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- ... Your existing HTML code ... -->
<script>
$(document).ready(function() {
    $('.noTripCard').hide();

    // Function to filter trips based on selected driver name
    function filterTripsByDriver(driverName) {
        var matchingTripFound = false;
        var no_trip =  $('.trip-card:first .user-name').text().trim();
        $('.trip-card').show();

        if (driverName != '' && no_trip != '') {
            $('.trip-card').each(function() {
                var tripDriverName = $(this).find('.user-name').text().trim();
                if (tripDriverName !== driverName) {
                    $(this).hide();
                } else {
                    matchingTripFound = true;
                }
            });
        }else{
            matchingTripFound = true;
        }

        if (matchingTripFound) {
            $('.noTripCard').hide();
        } else {
            $('.noTripCard').show();
        }
    }

    $('#statusSelect').change(function() {
        var selectedDriverName = $(this).val();
        filterTripsByDriver(selectedDriverName);
    });
});
</script>